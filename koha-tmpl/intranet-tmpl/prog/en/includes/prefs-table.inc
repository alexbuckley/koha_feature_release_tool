    [% FOREACH TAB IN TABS %]
    <div class="prefs-tab">
    <h2>[% TAB.tab_title %] preferences</h2>
    <form action="/cgi-bin/koha/admin/preferences.pl" method="post">
        [% UNLESS ( searchfield ) %]<div id="toolbar"><button class="save-all submit" type="submit">Save all [% TAB.tab_title %] preferences</button></div>[% END %]
        <input type="hidden" name="op" value="save" />
        <input type="hidden" name="tab" value="[% TAB.tab_id %]" />

            [% FOREACH LINE IN TAB.LINES %]
            [% IF ( LINE.is_group_title ) %]
            [% UNLESS ( loop.first ) %]</tbody></table>[% END %]
            <h3>[% LINE.title %]</h3>
            <table class="preferences">
            <thead><tr><th>Preference</th><th>Value</th></tr></thead>
            [% UNLESS ( loop.last ) %]<tbody>[% END %]
            [% ELSE %]
            [% IF ( loop.first ) %]<table class="preferences"><thead><tr><th>Preference</th><th>Value</th></tr></thead><tbody>[% END %]
            <tr class="name-row">
                <td class="name-cell">
                    <code>
                        [% FOREACH NAME IN LINE.NAMES %]
						<label for="pref_[% NAME.name %]">
							[% IF ( NAME.jumped ) %]
							<span class="term" id="jumped">[% NAME.name %]</span>
                            [% ELSIF ( NAME.highlighted ) %]
							<span class="term">[% NAME.name %]</span>
							[% ELSE %]
							[% NAME.name %]
							[% END %]

                            [% IF NAME.overridden %]
                                <span class="overridden" title="The system preference [% NAME.name %] may have been overridden from this value by one or more virtual hosts.">
                                    [Overridden]
                                </span>
                            [% END %]
						</label>
                        [% UNLESS ( loop.last ) %]<br />[% END %]
                        [% END %]
                    </code>
                </td>
                <td><div>
                    [% FOREACH CHUNK IN LINE.CHUNKS %]
                    [% IF ( CHUNK.type_text ) %]
                    [% CHUNK.contents %]
                    [% ELSIF ( CHUNK.type_input ) %]
                    <input type="[%IF CHUNK.input_type %][% CHUNK.input_type %][% ELSE %]text[% END %]" name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]" class="preference preference-[% CHUNK.class or "short" %]" value="[% CHUNK.value| html %]" autocomplete="off" /> [% IF ( CHUNK.dateinput ) %]<span class="hint">[% INCLUDE 'date-format.inc' %]</span>[% END %]
                    [% ELSIF ( CHUNK.type_select ) %]
                    <select name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]" class="preference preference-[% CHUNK.class or "choice" %]">
                        [% FOREACH CHOICE IN CHUNK.CHOICES.sort('value') %]
                        [% IF ( CHOICE.selected ) %]
                        <option value="[% CHOICE.value %]" selected="selected">
                        [% ELSE %]
                        <option value="[% CHOICE.value %]">
                        [% END %]
                            [% CHOICE.text %]
                        </option>
                        [% END %]
                    </select>
                    [% ELSIF ( CHUNK.type_multiple ) %]
                    <select name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]" class="preference preference-[% CHUNK.class or "choice" %]" multiple="multiple">
                        [% FOREACH CHOICE IN CHUNK.CHOICES %][% IF ( CHOICE.selected ) %]<option value="[% CHOICE.value %]" selected="selected">[% ELSE %]<option value="[% CHOICE.value %]">[% END %][% CHOICE.text %]</option>[% END %]
                    </select>
                    [% ELSIF ( CHUNK.type_textarea ) || ( CHUNK.type_htmlarea )%]
                        [% IF ( CHUNK.type_htmlarea ) && ( Koha.Preference('UseWYSIWYGinSystemPreferences') ) %]
                        <textarea name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]" class="preference preference-[% CHUNK.class or "short" %] mce" rows="20" cols="60">[% CHUNK.value %]</textarea>
                        [% ELSE %]
                        <a class="expand-textarea" style="display: none" href="#">Click to Edit</a>
                        <textarea name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]" class="preference preference-[% CHUNK.class or "short" %]" rows="10" cols="40">[% CHUNK.value %]</textarea>
                        <a class="collapse-textarea" style="display:none" href="#">Click to collapse</br></a>
                        [% END %]
                    [% ELSIF ( CHUNK.type_languages ) %]
                    <dl>
                    [% FOREACH language IN CHUNK.languages %]
                        [% IF ( language.plural ) %]
                        <dt>
                            [% IF ( language.native_description ) %][% language.native_description %][% ELSE %][% language.rfc4646_subtag %][% END %]
                        </dt>
                        [% FOREACH sublanguages_loo IN language.sublanguages_loop %]
                        <dd>
                            <label for="pref_[% CHUNK.name %]_[% sublanguages_loo.rfc4646_subtag %]">[% sublanguages_loo.native_description %] [% sublanguages_loo.script_description %] [% sublanguages_loo.region_description %] [% sublanguages_loo.variant_description %]([% sublanguages_loo.rfc4646_subtag %])</label>
                            [% IF ( sublanguages_loo.enabled ) %]
                            <input value="[% sublanguages_loo.rfc4646_subtag %]" name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]_[% sublanguages_loo.rfc4646_subtag %]" type="checkbox" checked="checked" class="preference preference-checkbox"/>
                            [% ELSE %]
                            <input value="[% sublanguages_loo.rfc4646_subtag %]" name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]_[% sublanguages_loo.rfc4646_subtag %]" type="checkbox" class="preference preference-checkbox"/>
                            [% END %]
                        </dd>
                        [% END %]
                        [% ELSE %]
                        <dt>
                            <label for="pref_[% CHUNK.name %]_[% language.rfc4646_subtag %]">[% language.native_description %]([% language.rfc4646_subtag %])</label>
                            [% IF ( language.group_enabled ) %]
                            <input value="[% language.rfc4646_subtag %]" name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]_[% language.rfc4646_subtag %]" type="checkbox" checked="checked" class="preference preference-checkbox"/>
                            [% ELSE %]
                            <input value="[% language.rfc4646_subtag %]" name="pref_[% CHUNK.name %]" id="pref_[% CHUNK.name %]_[% language.rfc4646_subtag %]" type="checkbox" class="preference preference-checkbox"/>
                            [% END %]
                        </dt>
                        [% END %]
                    [% END %]
                    </dl>
                    [% END %]
                    [% END %]
                </div></td>
            </tr>
            [% IF ( loop.last ) %]</tbody></table>[% END %]
            [% END %]
        [% END %]
        [% IF screen == 'preferences' %]
            <fieldset class="action"><button class="save-all submit" type="submit">Save all [% TAB.tab_title %] preferences</button> <a href="/cgi-bin/koha/admin/preferences.pl" class="force_reload cancel">Cancel</a></fieldset>
        [% ELSIF screen == 'featurereleasetool' %]
            <fieldset class="action"><button class="save-all submit" type="submit">Update sysprefs [% TAB.tab_title %] preferences</button> <a href="/cgi-bin/koha/admin/preferences.pl" class="force_reload cancel    ">Cancel</a></fieldset>
        [% END %]
    </form>
    </div>
    [% END %]
