ALTER TABLE systempreferences ADD version varchar(12) DEFAULT NULL;
UPDATE systempreferences SET version="16.06.00.003" WHERE variable ="MaxItemsToProcessForBatchMod";
UPDATE systempreferences SET version="16.06.00.003" WHERE variable ="MaxItemsToDisplayForBatchDel";
UPDATE systempreferences SET version="16.06.00.004" WHERE variable ="OPACXSLTListsDisplay";
UPDATE systempreferences SET version="16.06.00.004" WHERE variable ="XSLTListsDisplay";
UPDATE systempreferences SET version="16.06.00.005" WHERE variable ="XSLTListsDisplay";
UPDATE systempreferences SET version="16.06.00.006" WHERE variable="RefundLostOnReturnControl";
UPDATE systempreferences SET version="16.06.00.007" WHERE variable="PatronQuickAddFields";
UPDATE systempreferences SET version="16.06.00.008" WHERE variable="CheckPrevCheckout";
UPDATE systempreferences SET version="16.06.00.009" WHERE variable="IntranetCatalogSearchPulldown";
UPDATE systempreferences SET version="16.06.00.010" WHERE variable="MaxOpenSuggestions";
UPDATE systempreferences SET version="16.06.00.011" WHERE variable="NovelistSelectStaffEnabled";
UPDATE systempreferences SET version="16.06.00.011" WHERE variable="NovelistSelectStaffView";
UPDATE systempreferences SET version="16.06.00.013" WHERE variable="OPACResultsLibrary";
UPDATE systempreferences SET version="16.06.00.015" WHERE variable="HoldsLog";
UPDATE systempreferences SET version="16.06.00.020" WHERE variable="SwitchOnSiteCheckouts";
UPDATE systempreferences SET version="16.06.00.027" WHERE variable="TrackLastPatronActivity";
UPDATE systempreferences SET version="16.06.00.021" WHERE variable="PatronSelfRegistrationEmailMustBeUnique";
UPDATE systempreferences SET version="16.06.00.023" WHERE variable="timeout";
UPDATE systempreferences SET version="16.06.00.025" WHERE variable="makePreviousSerialAvailable";
UPDATE systempreferences SET version="16.06.00.026" WHERE variable="PatronSelfRegistrationLibraryList";
UPDATE systempreferences SET version="16.06.00.027" WHERE variable="TrackLastPatronActivity";
UPDATE systempreferences SET version="16.06.00.029" WHERE variable="UsageStatsLibraryType";
UPDATE systempreferences SET version="16.06.00.029" WHERE variable="UsageStatsCountry";
UPDATE systempreferences SET version="16.06.00.030" WHERE variable="OPACHoldingsDefaultSortField";
UPDATE systempreferences SET version="16.06.00.031" WHERE variable="PatronSelfRegistrationPrefillForm";
UPDATE systempreferences SET version="16.06.00.035" WHERE variable="AllowItemsOnHoldCheckoutSCO";
UPDATE systempreferences SET version="16.06.00.036" WHERE variable="HouseboundModule";
UPDATE systempreferences SET version="16.06.00.037" WHERE variable="ArticleRequests";
UPDATE systempreferences SET version="16.06.00.037" WHERE variable="ArticleRequestsMandatoryFields";
UPDATE systempreferences SET version="16.06.00.037" WHERE variable="ArticleRequestsMandatoryFieldsItemsOnly";
UPDATE systempreferences SET version="16.06.00.037" WHERE variable="ArticleRequestsMandatoryFieldsRecordOnly";
UPDATE systempreferences SET version="16.06.00.038" WHERE variable="DefaultPatronSearchFields";
UPDATE systempreferences SET version="16.06.00.041" WHERE variable="AggressiveMatchOnISSN";
UPDATE systempreferences SET version="16.06.00.045" WHERE variable="BorrowerRenewalPeriodBase";
UPDATE systempreferences SET version="16.06.00.049" WHERE variable="ReplytoDefault";
UPDATE systempreferences SET version="16.06.00.049" WHERE variable="ReturnpathDefault";
UPDATE systempreferences SET version="16.12.00.005" WHERE variable="AuthorityMergeMode";
UPDATE systempreferences SET version="16.12.00.008" WHERE variable="MarcItemFieldsToOrder";
UPDATE systempreferences SET version="16.12.00.009" WHERE variable="OPACHoldsIfAvailableAtPickup";
UPDATE systempreferences SET version="16.12.00.009" WHERE variable="OPACHoldsIfAvailableAtPickupExceptions";
UPDATE systempreferences SET version="16.12.00.010" WHERE variable="OverDriveCirculation";
UPDATE systempreferences SET version="16.12.00.012" WHERE variable="OpacNewsLibrarySelect";
UPDATE systempreferences SET version="16.12.00.013" WHERE variable="CircSidebar";
UPDATE systempreferences SET version="16.12.00.014" WHERE variable="LoadSearchHistoryToTheFirstLoggedUser";
UPDATE systempreferences SET version="16.12.00.015" WHERE variable="UsageStatsGeolocation";
UPDATE systempreferences SET version="16.12.00.015" WHERE variable="UsageStatsLibrariesInfo";
UPDATE systempreferences SET version="16.12.00.015" WHERE variable="UsageStatsPublicID";
UPDATE systempreferences SET version="16.12.00.017" WHERE variable="CumulativeRestrictionPeriods";
UPDATE systempreferences SET version="16.12.00.020" WHERE variable="HoldFeeMode";
UPDATE systempreferences SET version="16.12.00.021" WHERE variable="RenewalLog";
UPDATE systempreferences SET version="16.12.00.023" WHERE variable="AuthorityMergeLimit";
UPDATE systempreferences SET version="16.12.00.024" WHERE variable="OverdueNoticeBcc";
UPDATE systempreferences SET version="16.12.00.025" WHERE variable="UploadPurgeTemporaryFilesDays";
UPDATE systempreferences SET version="16.12.00.028" WHERE variable="AddressFormat";
UPDATE systempreferences SET version="16.12.00.029" WHERE variable="AllowCheckoutNotes";
UPDATE systempreferences SET version="16.12.00.032" WHERE variable="ExcludeHolidaysFromMaxPickUpDelay";
UPDATE systempreferences SET version="16.12.00.033" WHERE variable="TranslateNotices";
UPDATE systempreferences SET version="16.12.00.034" WHERE variable="OPACFineNoRenewalsBlockAutoRenew";
UPDATE systempreferences SET version="16.12.00.036" WHERE variable="NumSavedReports";
UPDATE systempreferences SET version="16.12.00.037" WHERE variable="FailedLoginAttempts";
UPDATE systempreferences SET version="16.12.00.038" WHERE variable="ExportRemoveFields";
UPDATE systempreferences SET version="16.12.00.039" WHERE variable="TalkingTechItivaPhoneNotification";

















