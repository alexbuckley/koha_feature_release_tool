<div id="team">
   <h2>Special thanks to the following organizations</h2>
   <ul>
       <li><a href="http://library.org.nz">Horowhenua Library Trust</a>, New Zealand, and Rosalie Blake, Head of Libraries, (Koha 1.0)</li>
       <li>The <a href="http://www.myacpl.org/">Athens County Public Libraries</a>, Ohio, USA (MARC sponsorship, documentation, template maintenance)</li>
       <li><a href="http://www.emn.fr">EMN (Ecole des Mines de Nantes)</a>, France (Suggestions, Stats wizards and improved LDAP sponsorship)</li>
       <li><a href="http://www.mines-paristech.fr">Mines Paristech (previously Ecole Nationale Supérieure des Mines de Paris)</a>, France (biblio frameworks, MARC authorities, OPAC basket, Serials sponsorship)</li>
       <li><a href="http://www.mediathequeouestprovence.fr/">SAN-Ouest Provence</a>, France (Koha 3.0 enhancements to patrons and holds modules)</li>
       <li>The <a href="http://ccfls.org">Crawford County Federated Library System</a>, PA, USA (Koha 3.0 Zebra Integration sponsorship)</li>
       <li>The <a href="http://www.geauga.lib.oh.us/">Geauga County Public Library</a>, OH, USA (Koha 3.0 beta testing)</li>
       <li>The <a href="http://library.neu.edu.tr">Near East University</a>, Cyprus</li>
       <li>OPUS International Consultants, Wellington, New Zealand (Corporate Serials sponsorship)</li>
       <li><a href="http://www.famfamfam.com/">famfamfam.com</a> Birmingham (UK) based developer Mark James for the famfamfam Silk iconset.</li>
       <li><a href="http://www.ashs.school.nz/">Albany Senior High School</a>, Auckland, New Zealand (OPAC 'star-ratings' sponsorship)</li>
   </ul>

   <h2>Koha <span style="color:red" title="Koha version numbering has jumped from 3.22 to 16.05 (yy.mm) as from May 2016">17.05</span> release team</h2>
   <ul>
       <li><strong>Release manager:</strong>
                    <a href="https://www.openhub.net/p/koha/contributors/6620692261494">Kyle Hall</a>, <a href="https://www.openhub.net/p/koha/contributors/6618544661344">Brendan Gallagher</a></li>
       <li><strong>(Database) Documentation manager:</strong>
           <a href="https://www.openhub.net/p/koha/contributors/6618544609030">Chris Cormack</a>, David Nind (Assistant)
       </li>
       <li><strong>Translation manager:</strong>
           <a href="https://www.openhub.net/p/koha/contributors/6618544839606">Bernardo González Kriegel</a></li>
       <li><strong>Quality assurance team:</strong>
        <ul>
          <li><a href="https://www.openhub.net/p/koha/contributors/6618544730094">Tomás Cohen Arazi</a></li>
          <li><a href="https://www.openhub.net/p/koha/contributors/6620692886191">Nick Clemens</a></li>
          <li><a href="https://www.openhub.net/p/koha/contributors/6618545125093">Jonathan Druart</a></li>
          <li><a href="https://www.openhub.net/p/koha/contributors/6620692261494">Kyle Hall</a></li>
          <li><a href="https://www.openhub.net/p/koha/contributors/6620692419690">Julian Maurice</a></li>
          <li><a href="https://www.openhub.net/p/koha/contributors/6618544785220">Martin Renvoize</a></li>
          <li><a href="https://www.openhub.net/p/koha/contributors/6618544727712">Marcel de Rooy</a></li>
          <li><a href="https://www.openhub.net/p/koha/contributors/6620692831733">Fridolin Somers</a></li>
        </ul>
       </li>
       <li><strong>Release maintainers:</strong>
        <ul>
         <li><a href="https://www.openhub.net/p/koha/contributors/6618544670742">Katrin Fischer</a> (16.11)</li>
         <li><a href="https://www.openhub.net/p/koha/contributors/6618544618401">Mason James</a> (16.05)</li>
         <li><a href="https://www.openhub.net/p/koha/contributors/6620692419690">Julian Maurice</a> (3.22)</li>
        </ul>
       </li>
       <li><strong>Packaging manager:</strong>
         <a href="https://www.openhub.net/p/koha/contributors/6620692605913">Mirko Tietgen</a>
       </li>
       <li><strong>Jenkins maintainer:</strong>
         <a href="https://www.openhub.net/p/koha/contributors/6618544730094">Tomás Cohen Arazi</a>
       </li>
       <li><strong>Bug wranglers:</strong>
          <ul>
              <li>Indranil Das Gupta</li>
              <li><a href="https://www.openhub.net/p/koha/contributors/6618545408147">Marc Véron</a></li>
          </ul>
       </li>
   </ul>

   <h2>Koha development team</h2>
     <ul>
       <li>Jacek Ablewicz</li>
       <li>Md. Aftabuddin</li>
       <li>Jon Aker</li>
       <li>Chloe Alabaster</li>
       <li>Edward Allen</li>
       <li>Francisco M. Marzoa Alonso</li>
       <li>Joseph Alway</li>
       <li>Cindy Murdock Ames</li>
       <li>Aleisha Amohia</li>
       <li>Roman Amor</li>
       <li>Richard Anderson</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692181851">Nahuel Angelinetti</a></li>
       <li>Nuño López Ansótegui</li>
       <li>Dimitris Antonakis</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544730094">Tomás Cohen Arazi (3.18 - 3.22 Release Manager; 3.12 Release Maintainer; 16.05, 16.11 QA Team Member)</a></li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692124662">Andrew Arensburger (the small and great C4::Context module)</a></li>
       <li>Alex Arnaud</li>
       <li>Petter Goksoyr Asen</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544614259">Joe Atzberger</a></li>
       <li>Héctor Eduardo Castro Avalos</li>
       <li>Larry Baerveldt</li>
       <li>Marc Balmer</li>
       <li>Edmund Balnaves</li>
       <li>Al Banks</li>
       <li>Daniel Banzli</li>
       <li>Stefano Bargioni</li>
       <li>Daniel Barker</li>
       <li>Greg Barniskis</li>
       <li>Benedykt P. Barszcz (Polish for 2.0)</li>
       <li>D Ruth Bavousett (3.12 Translation Manager)</li>
       <li>Maxime Beaulieu</li>
       <li>Natalie Bennison</li>
       <li>John Beppu</li>
       <li>Pablo Bianchi</li>
       <li>David Birmingham</li>
       <li>Florian Bischof</li>
       <li>Gaetan Boisson</li>
       <li>Danny Bouman</li>
       <li>Christopher Brannon (3.20 QA Team Member)</li>
       <li>Stan Brinkerhoff</li>
       <li>Isaac Brodsky</li>
       <li>Ivan Brown</li>
       <li>Roger Buck</li>
       <li>Steven Callender</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692376789">Jared Camins-Esakov (3.12 Release Manager; 3.6 Release Maintainer)</a></li>
       <li>Colin Campbell (3.4 QA Manager)</li>
       <li>Fernando Canizo</li>
       <li>Barry Cannon</li>
       <li>Frédérick Capovilla</li>
       <li>DeAndre Carroll</li>
       <li>Chris Catalfo (new plugin MARC editor)</li>
       <li>Marc Chantreux</li>
       <li>Jerome Charaoui</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544614260">Galen Charlton (3.2, 3.14, and 3.16 Release Manager; 3.16 Release Maintainer; 3.18 QA Team Member; 16.05 Packaging Manager)</a></li>
       <li>Francois Charbonnier</li>
       <li>Evonne Cheung</li>
       <li>Andrew Chilton</li>
       <li>Barton Chittenden</li>
       <li>Koha SAB CINECA</li>
       <li>Nick Clemens (16.11 QA Team Member)</li>
       <li>Garry Collum</li>
       <li>David Cook</li>
       <li>John Copeland</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544609030">Chris Cormack (1.x, 3.4 and 3.6 Release Manager; 3.8, 3.10, 3.18 and 3.20 Release Maintainer; 3.2 Translation Manager; 3.14 QA Team Member)</a></li>
       <li>Jeremy Crabtree</li>
       <li>Samuel Crosby</li>
       <li>Christophe Croullebois</li>
       <li>Olivier Crouzet</li>
       <li>Nate Curulla</li>
       <li>Vincent Danjean</li>
       <li>Hugh Davenport</li>
       <li>Elliott Davis (3.12 QA Team Member)</li>
       <li>Doug Dearden</li>
       <li>Kip DeGraaf</li>
       <li>Stéphane Delaune</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692210484">Frédéric Demians (3.4 - 3.10 Translation Manager; 3.20, 16.05 Release Maintainer)</a></li>
       <li>Connor Dewar</li>
       <li>Srikanth Dhondi</li>
       <li>Rocio Dressler</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618545125093">Jonathan Druart (3.8 - 16.11 QA Team Member)</a></li>
       <li>Serhij Dubyk</li>
       <li>Yohann Dufour</li>
       <li>Thomas Dukleth (MARC Frameworks Maintenance)</li>
       <li>Frederic Durand</li>
       <li>Sebastiaan Durand</li>
       <li>Rachel Dustin</li>
       <li>Ecole des Mines de Saint Etienne, Philippe Jaillon (OAI-PMH support)</li>
       <li>Stephen Edwards</li>
       <li>Dani Elder</li>
       <li>Gus Ellerm</li>
       <li>Andrew Elwell</li>
       <li>Brian Engard</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544646984">Nicole C. Engard (3.0 - 16.11 Documentation Manager)</a></li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544677502">Magnus Enger</a></li>
       <li>Esiee School (Jérome Vizcaino, Michel Lerenard, Pierre Cauchois)</li>
       <li>Jason Etheridge</li>
       <li>Shaun Evans</li>
       <li>Pat Eyler (Kaitiaki from 2002 to 2004)</li>
       <li>Charles Farmer</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544609865">Antoine Farnault</a></li>
       <li>Arslan Farooq</li>
       <li>Vitor Fernandes</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544609053">Joshua Ferraro (3.0 Release Manager and Translation Manager)</a></li>
       <li>Julian Fiol</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544670742">Katrin Fischer (3.12 - 16.11 QA Manager)</a></li>
       <li>Connor Fraser</li>
       <li>Clay Fouts</li>
       <li>Brendon Ford</li>
       <li>Claudia Forsman</li>
       <li>Corey Fuimaono</li>
       <li>Marco Gaiarin</li>
       <li>Pierrick Le Gall</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544661344">Brendan A. Gallagher (3.14 - 3.22 QA Team Member; 16.05, 16.11 Release Manager)</a></li>
       <li>Tumer Garip</li>
       <li>Russel Garlick</li>
       <li>Mark Gavillet</li>
       <li>Claire Gravely</li>
       <li>Daniel Kahn Gillmor</li>
       <li>David Goldfein</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544839606">Bernardo González Kriegel (3.14 - 16.11 Translation Manager; 3.10 Release Maintainer)</a></li>
       <li>Briana Greally</li>
       <li>Daniel Grobani</li>
       <li>Amit Gupta</li>
       <li>Indranil Das Gupta</li>
       <li>Michael Hafen</li>
       <li>Christopher Hall (3.8 Release Maintainer)</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692261494">Kyle Hall (3.8 Release Maintainer; 3.14 - 16.11 QA Team Member; 16.11 Release Manager)</a></li>
       <li>Sean Hamlin</li>
       <li>Tim Hannah</li>
       <li>Mike Hansen</li>
       <li>Brian Harrington</li>
       <li>Brandon Haveman</li>
       <li>Rochelle Healy</li>
       <li>Emma Heath</li>
       <li>Friedrich zur Hellen</li>
       <li>Kate Henderson</li>
       <li>Michaes Herman</li>
       <li>Claire Hernandez</li>
       <li>Wolfgang Heymans</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544614261">Ryan Higgins</a></li>
       <li>Morag Hills</li>
       <li>Sèbastien Hinderer</li>
       <li>Kristina D.C. Hoeppner</li>
       <li>Stephanie Hogan</li>
       <li>Karl Holten</li>
       <li>Daniel Holth</li>
       <li>Andrew Hooper</li>
       <li>Alexandra Horsman</li>
       <li>Tom Houlker</li>
       <li>Matthew Hunt</li>
       <li>Christopher Hyde</li>
       <li>Rolando Isidoro</li>
       <li>Cory Jaeger</li>
       <li>Lee Jamison</li>
       <li>Srdjan Jankovic</li>
       <li>Philippe Jaillon</li>
       <li><a href="https://www.openhub.net/accounts/kohaaloha">Mason James (3.10 - 3.14 QA Team Member, 3.16 Release Maintainer)</a></li>
       <li>Mike Johnson</li>
       <li>Donovan Jones</li>
       <li>Bart Jorgensen</li>
       <li>Janusz Kaczmarek</li>
       <li>Koustubha Kale</li>
       <li>Pasi Kallinen</li>
       <li>Peter Crellan Kelly</li>
       <li>Jorgia Kelsey</li>
       <li>Olli-Antti Kivilahti</li>
       <li>Attila Kinali</li>
       <li>Chris Kirby</li>
       <li>Ulrich Kleiber</li>
       <li>Rafal Kopaczka</li>
       <li>Piotr Kowalski</li>
       <li>Joonas Kylmälä</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544614275">Henri-Damien Laurent (3.0 Release Maintainer)</a></li>
       <li>Arnaud Laurin</li>
       <li>Nicolas Legrand</li>
       <li>Sonia Lemaire</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544609147">Owen Leonard (3.0+ Interface Design)</a></li>
       <li>Ambrose Li (translation tool)</li>
       <li>Gynn Lomax</li>
       <li>Peter Lorimer</li>
       <li>Robert Lyon (Corporate Serials)</li>
       <li>Merllisia Manueli</li>
       <li>Francois Marier</li>
       <li>Patricio Marrone</li>
       <li>Jesse Maseto</li>
       <li>Frère Sébastien Marie</li>
       <li>Ricardo Dias Marques</li>
       <li>Julian Maurice (3.18 QA Team Member; 3.22 Release Maintainer)</li>
       <li>Remi Mayrand-Provencher</li>
       <li>Brig C. McCoy</li>
       <li>Tim McMahon</li>
       <li>Dorian Meid (German translation)</li>
       <li>Meenakshi. R</li>
       <li>Melia Meggs</li>
       <li>Holger Meißner</li>
       <li>Karl Menzies</li>
       <li>Matthias Meusburger</li>
       <li>Sophie Meynieux</li>
       <li>Janet McGowan</li>
       <li>Alan Millar</li>
       <li>Jono Mingard</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544607803">Andrew Moore</a></li>
       <li>Francesca Moore</li>
       <li>Josef Moravec</li>
       <li>Sharon Moreland</li>
       <li>Nicolas Morin</li>
       <li>Mike Mylonas</li>
       <li>Natasha ?? [Catalyst Academy]</li>
       <li>Nadia Nicolaides</li>
       <li>Joy Nelson</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544615991">Chris Nighswonger (3.2 - 3.6 Release Maintainer)</a></li>
       <li>Brian Norris</li>
       <li>Duy Tinh Nguyen</li>
       <li>Simith D'Oliveira</li>
       <li>Albert Oller</li>
       <li>Eric Olsen</li>
       <li>H. Passini</li>
       <li>Aliki Pavlidou</li>
       <li>Dobrica Pavlinusic</li>
       <li>Maxime Pelletier</li>
       <li>Shari Perkins</li>
       <li>Martin Persson</li>
       <li>Fred Pierre</li>
       <li>Genevieve Plantin</li>
       <li>Polytechnic University</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544612249">Paul Poulain (2.0, 2.2, 3.8, 3.10 Release Manager; 2.2 Release Maintainer; 3.12 - 16.05 QA Team Member)</a></li>
       <li>Karam Qubsi</li>
       <li>Romina Racca</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692116417">MJ Ray (2.0 Release Maintainer)</a></li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544650708">Liz Rea</a> (3.6, 3.18 Release Maintainer)</li>
       <li>Thatcher Rea</li>
       <li>Allen Reinmeyer</li>
       <li>Serge Renaux</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544785220">Martin Renvoize (3.16 - 16.11 QA Team Member)</a></li>
       <li>Abby Robertson</li>
       <li>Waylon Robertson</li>
       <li>Benjamin Rokseth</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544727712">Marcel de Rooy (3.8 - 16.11 QA Team Member)</a></li>
       <li>Andreas Roussos</li>
       <li>Salvador Zaragoza Rubio</li>
       <li>Mathieu Saby</li>
       <li>Eivin Giske Skaaren</li>
       <li>Brice Sanchez</li>
       <li>Sam Sanders</li>
       <li>Rodrigo Santellan</li>
       <li>Viktor Sarge</li>
       <li>A. Sassmannshausen</li>
       <li>Adrien Saurat</li>
       <li>Dan Scott</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544619438">Robin Sheat (3.2 - 3.22 Packaging Manager)</a></li>
       <li>Juhani Seppälä</li>
       <li>John Seymour</li>
       <li>Juan Romay Sieira</li>
       <li>Zach Sim</li>
       <li>Radek Siman</li>
       <li>Silvia Simonetti</li>
       <li>Savitra Sirohi</li>
       <li>Pawel Skuza (Polish for 1.2)</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692831733">Fridolin Somers (3.14 Release Maintainer)</a></li>
       <li>Southeastern University</li>
       <li>Martin Stenberg</li>
       <li>Glen Stewart</li>
       <li>Will Stokes</li>
       <li>Simon Story</li>
       <li>David Strainchamps</li>
       <li>Ed Summers (Some code and Perl packages like MARC::Record)</li>
       <li>Daniel Sweeney</li>
       <li>Zeno Tajoli</li>
       <li>Lari Taskula</li>
       <li>Samanta Tello</li>
       <li>Adam Thick</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618544609107">Finlay Thompson</a></li>
       <li>Peggy Thrasher</li>
       <li>Fabio Tiana</li>
       <li>Mirko Tietgen (16.11 Packaging Manager)</li>
       <li>Mark Tompsett</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692101577">Steve Tonnesen (early MARC work, Virtual Bookshelves concept, KohaCD)</a></li>
       <li>Bruno Toumi</li>
       <li>Andrei V. Toutoukine</li>
       <li>Duncan Tyler</li>
       <li>Kathryn Tyree</li>
       <li>Darrell Ulm</li>
       <li>Universidad ORT Uruguay (Ernesto Silva, Andres Tarallo)</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6618545408147">Marc Véron</a></li>
       <li>Justin Vos</li>
       <li>Aleksa Vujicic</li>
       <li>Reed Wade</li>
       <li>Stacey Walker</li>
       <li>Ian Walls (3.6 - 3.10 QA Manager)</li>
       <li><a href="https://www.openhub.net/accounts/janewagner">Jane Wagner</a></li>
       <li>Ward van Wanrooij</li>
       <li><a href="https://www.openhub.net/accounts/pianohacker">Jesse Weaver (16.05, 16.11 QA Team Member)</a></li>
       <li>Stefan Weil</li>
       <li>Aaron Wells</li>
       <li>Rick Welykochy</li>
       <li>Piotr Wejman</li>
       <li>Ron Wickersham</li>
       <li>Brett Wilkins</li>
       <li><a href="https://www.openhub.net/p/koha/contributors/6620692127299">Olwen Williams (Database design and data extraction for Koha 1.0)</a></li>
       <li>Robert Williams</li>
       <li>James Winter</li>
       <li>Lars Wirzenius</li>
       <li>Thomas Wright</li>
       <li>Jen Zajac</li>
       <li>Kenza Zaki</li>
     </ul>
     <h3>Contributing companies and institutions</h3>
     <ul>
       <li>BibLibre, France</li>
       <li>Bibliotheksservice-Zentrum Baden-Württemberg (BSZ), Germany</li>
       <li>ByWater Solutions, USA</li>
       <li>Calyx, Australia</li>
       <li>Catalyst IT, New Zealand</li>
       <li>C &amp; P Bibliography Services, USA</li>
       <li>Hochschule für Gesundheit (hsg), Germany</li>
       <li>Katipo Communications, New Zealand</li>
       <li>KEEP SOLUTIONS, Portugal</li>
       <li>KohaAloha, New Zealand</li>
       <li>LibLime, USA</li>
       <li>Libriotech, Norway</li>
       <li>Nelsonville Public Library, Ohio, USA</li>
       <li>Prosentient Systems, Australia</li>
       <li>PTFS, Maryland, USA</li>
       <li>PTFS Europe Ltd, United Kingdom</li>
       <li>Rijksmuseum, Amsterdam, The Netherlands</li>
       <li>SAN-Ouest Provence, France</li>
       <li>software.coop, United Kingdom</li>
       <li>Tamil, France</li>
       <li>Universidad Nacional de Córdoba, Argentina</li>
       <li>Xercode, Spain</li>
     </ul>

     <h2>Additional thanks to...</h2>
       <ul>
         <li>Irma Birchall</li>
         <li>Rachel Hamilton-Williams (Kaitiaki from 2004 to present)</li>
         <li>Stephen Hedges (early Documentation Manager)</li>
         <li>Brooke Johnson</li>
         <li>Jo Ransom</li>
         <li>Nicholas Rosasco (Documentation Compiler)</li>
         <li>Regula Sebastiao</li>
       </ul>
</div>
