#!/usr/bin/perl

# This file is part of Koha.
#
# Copyright (C) 2017  Catalyst IT
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use strict;
use warnings;
use diagnostics;

use C4::InstallAuth;
use CGI qw ( -utf8 );
use POSIX qw(strftime);

use C4::Context;
use C4::Output;
use C4::Templates;
use C4::Languages qw(getAllLanguages getTranslatedLanguages);
use C4::Installer;
use C4::NewsChannels;
use Koha;
use Data::Dumper;
use Koha::DateUtils;
use Date::Calc qw/Date_to_Days Today/;
use C4::NewsChannels;


my $query = new CGI;
my $step  = $query->param('step');
my $kohaversion = $query->param('kohaversion'); #Retrieve kohaversion number from url
my $dbversion = $query->param('dbversion');
my $dbh = C4::Context->dbh;
my $op = $query->param('op');
my $language = $query->param('language');
my ( $template, $loggedinuser, $cookie );

my $all_languages = getAllLanguages();

if ( defined($language) ) {
    C4::Templates::setlanguagecookie( $query, $language, "install.pl?step=1" );
}

#Set featurereleasetool.tt as template
( $template, $loggedinuser, $cookie ) = get_template_and_user(
    {
        template_name => "installer/featurereleasetool.tt",
        query         => $query,
        type          => "intranet",
        authnotrequired => 0,
        debug           => 1,
    }
);

#Database settings
my $installer = C4::Installer->new();
my %info;
$info{'dbname'} = C4::Context->config("database");
$info{'dbms'}   = (
      C4::Context->config("db_scheme")
    ? C4::Context->config("db_scheme")
    : "mysql"
);
$info{'hostname'} = C4::Context->config("hostname");
$info{'port'}     = C4::Context->config("port");
$info{'user'}     = C4::Context->config("user");
$info{'password'} = C4::Context->config("pass");
$info{'tls'} = C4::Context->config("tls");
    if ($info{'tls'} && $info{'tls'} eq 'yes'){
        $info{'ca'} = C4::Context->config('ca');
        $info{'cert'} = C4::Context->config('cert');
        $info{'key'} = C4::Context->config('key');
        $info{'tlsoptions'} = ";mysql_ssl=1;mysql_ssl_client_key=".$info{key}.";mysql_ssl_client_cert=".$info{cert}.";mysql_ssl_ca_file=".$info{ca};
        $info{'tlscmdline'} =  " --ssl-cert ". $info{cert} . " --ssl-key " . $info{key} . " --ssl-ca ".$info{ca}." "
    }

# To be deleted
#my $dbh = DBI->connect(
#   "DBI:$info{dbms}:dbname=$info{dbname};host=$info{hostname}"
#     . ( $info{port} ? ";port=$info{port}" : "" )
#     . ( $info{tlsoptions} ? $info{tlsoptions} : "" ),
#   $info{'user'}, $info{'password'}
#);

my $location = "feature";
#Hand kohaversion and dbversion to template
$template->param(
        'kohaversion' => $kohaversion,
        'dbversion'   => $dbversion,
        'location'    => $location,
);

my $logdir = C4::Context->config('logdir');
my @logs = `cd $logdir && ls -t | grep "updatedatabase_2*"`;

my $filename_suffix = $logs[0];
my $path = $logdir . '/' . $filename_suffix;
my $fh;
my $file;
my $count = 0;
my $version;
$file = `cat $path`;
$file = `echo "$file" | grep -Eo '?[0-9]+[.][0-9]+[.][0-9]+[.][0-9]+?'`;

my @prefsloop;
my @lines = split /\n/, $file;
foreach my $line (@lines) {
    my $query = qq|
       SELECT variable, value, options, explanation from systempreferences WHERE version= ? |;
    my $sth = $dbh->prepare($query);
    $sth->execute($line);
    while (my ($variable, $value, $options, $explanation) =$sth->fetchrow) {
        push @prefsloop, {
            variable => $variable,
            value    => $value,
            options  => $options,
            explanation => $explanation,
        };
    }
};

$template->param( prefs => \@prefsloop );
warn $op;
#If the user has finished using the feature release change tool then redirect to mainpage
if ( $op && $op eq 'finished' ) {
    print $query->redirect("/cgi-bin/koha/mainpage.pl");
    exit;
}
elsif ( $op && $op eq 'save' ) { #Submit changed systempreferences 
    unless ( C4::Context->config( 'demo' ) ) {
        foreach my $param ( $query->param() ) {
            my ( $prefname ) = ( $param =~ /pref_(.*)/ );
            next if ( !defined( $prefname ) );

            my $wholevalue = join( ',', $query->param($param) );
            my ($name, $value) = split(/,/,$wholevalue);
            C4::Context->set_preference( $name, $value );
        }
    }
}


#Submit news item 
my $id      = $query->param('id');
my $title   = $query->param('title');
my $content = $query->param('featurecontent');
warn $content;
my $expirationdate;
if ( $query->param('expirationdate') ) {
    $expirationdate = output_pref({ dt => dt_from_string( scalar $query->param('expirationdate') ), dateformat => 'iso', dateonly => 1 }    );
}
my $timestamp = output_pref({ dt => dt_from_string( scalar $query->param('timestamp') ), dateformat => 'iso', dateonly => 1 });
my $number = $query->param('number');
my $lang   = $query->param('lang');
my $branchcode = $query->param('branch');

my $error_message  = $query->param('error_message');
$branchcode = undef if (defined($branchcode) && $branchcode eq '');

$template->param( error_message => $error_message ) if $error_message;

my $new_detail = get_opac_new($id);

if ($title) {
   my $newsposting =  add_opac_new(
      {
           title           => $title,
           content         => $content,
           lang            => $lang,
           expirationdate  => $expirationdate,
           timestamp       => $timestamp,
           number          => $number,
           branchcode      => $branchcode,
           borrowernumber  => $loggedinuser,
      }
   );
   if ($newsposting) {
       $template->param( newsposted => $newsposting );
   }
}



output_html_with_http_headers $query, $cookie, $template->output;

