<div>
    [% IF location == "feature" %]
        <form name="add_form" id="add_form" method="post" action="/cgi-bin/koha/installer/featurereleasetool.pl?dbversion=[%dbversion%]&kohaversion=[%kohaversion%]" onsubmit="sbmt()">
    [% ELSIF location == "intranet" %]
        <form name="add_form" method="post" action="/cgi-bin/koha/tools/koha-news.pl" >
    [% END %]
    <input type="hidden" name="op" value="[% op %]" />
    <input type="hidden" name="id" value="[% id %]" />
    <input type="hidden" name="dbversion" value="[% dbversion %]" />
    <input type="hidden" name="kohaversion" value="[% kohaversion %]" />
			<fieldset class="rows">
            <legend>OPAC and Koha news</legend>
           <ol> <li>
            <label for="lang">Display location:</label>
            <select id="lang" name="lang">
                [% IF ( default_lang == "" ) %]
                <option value="" selected="selected">All</option>
                [% ELSE %]
                <option value=""                    >All</option>
                [% END %]
                [% IF ( default_lang == "koha" ) %]
                <option value="koha" selected="selected">Librarian interface</option>
                [% ELSE %]
                <option value="koha"                    >Librarian interface</option>
                [% END %]
                [% IF ( default_lang == "slip" ) %]
                <option value="slip" selected="selected">Slip</option>
                [% ELSE %]
                <option value="slip"                    >Slip</option>
                [% END %]
                [% FOREACH lang_lis IN lang_list %]
                [% IF ( lang_lis.language == default_lang ) %]
                    <option value="[% lang_lis.language %]" selected="selected">OPAC ([% lang_lis.language %])</option>
                [% ELSE %]
                    <option value="[% lang_lis.language %]" >OPAC ([% lang_lis.language %])</option>
                [% END %]
                [% END %]
            </select>
            </li>
            <li>
                <label for="branch">Library: </label>
                <select id="branch" name="branch">
                    [% IF ( new_detail.branchcode == '' ) %]
                        <option value="" selected="selected">All libraries</option>
                    [% ELSE %]
                        <option value="">All libraries</option>
                    [% END %]
                </select>
            </li>
            <li>
                <label for="title" class="required">Title: </label>
                <input id="title" size="30" type="text" name="title" value=[% IF location=="feature" %] "Our library has just updated to [% kohaversion %]" [% END %]"[% new_detail.title %]" required="required" class="required" /> <span class="required">Required</span>
            </li>
            <li>
                <label for="from">Publication date: </label>
                <input id="from" type="text" name="timestamp" size="15" value="[% new_detail.timestamp %]" class="datepickerfrom" />
				<div class="hint">[% INCLUDE 'date-format.inc' %]</div>
            </li>
            <li>
                <label for="to">Expiration date: </label>
                <input id="to" type="text" name="expirationdate" size="15" value="[% new_detail.expirationdate %]" class="datepickerto" />
				<div class="hint">[% INCLUDE 'date-format.inc' %]</div>
            </li>
            <li>
                <label for="number">Appear in position: </label>
                [% IF ( new_detail.number ) %]
                    <input id="number" size="3" name="number" type="text" value="[% new_detail.number %]" />
                [% ELSE %]
                    <input id="number" size="3" name="number" type="text" />
                [% END %]
            </li>
            <li><label for="content">News: </label>
                [% IF location=="feature" %]
                <br><br>
                    <div contenteditable="true" name="featurecontent" id="featurecontent" cols="75" rows="10" >
                        Our institution is proud to announce that we have upgraded our Koha library management system to Koha version [%kohaversion%]. This new Koha version comes with many new exciting features.
                        <br>
                        <p>Read about the enhancements here: <a href="https://koha-community.org/koha-17-05-released/"> Koha 17.05 release notes link </a></p>
                        <p> During the upgrade the following new systempreferences were also installed:</p>
                        [% FOREACH pref IN prefs %]
                            [% pref.variable %]
                            [% pref.value %]
                        [% END %]
                    </div>
                    <div class="control" onclick="sbmt()"></div>
                [% ELSIF location=="intranet" %]
                    <textarea name="content" id="content" cols="75" rows="10">[% new_detail.content %]</textarea>
                [% END %]
            </li>
            </ol>
            <input class="button" type="submit" value="Submit" />
        </fieldset>
    </form>
</div>
